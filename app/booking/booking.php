
<?php
include("../share/check_session.php");
include("../share/dbconnection.php");
include("../share/header.php");
?>
<!DOCTYPE html>
<html lang="en">

<?php
if(isset($_GET['booking']))
{
    $room_id = $_GET['room_id'];
    $sql = "select * FROM room WHERE room_id = ' $room_id ' ";
    $runQuery = mysqli_query($con,$sql);
    while($row = $runQuery->fetch_assoc()) 
    {  
    ?>
    <body>
    <div class="container">
        <form action="check_booking.php" method='POST'>
            <div class="card">
                <div class="card-header"> 
                    <div class="form-group">
                        <input id="room_id" name="room_id" type="hidden" value="<?= $room_id  ?>">
                    </div>                     
                </div>
                <div class="card-body">
                    <img class="card-img-top" src="../../public/img/room.jpeg" alt="Room" style="width:100%; height:30% padding:2px;">
                    <div class="marginForButton"></div>
                    <h5 class="card-title">Detaje</h5>
                    <p class="card-text">Numri i dhomes: <?=$row['room_number']  ?></p>
                    <p class="card-text">Tipi i dhomes: <?=$row['type']  ?></p>
                    <p class="card-text">Cmimi i dhomes ne dollar: <?=$row['price']  ?></p>
                    <div class="row">
                        <div class="col-sm-6">
                            <h5>Data e fillimit te rezervimit</h5>
                            <div class="input-group">
                                <input type="date" id="checkin" name="checkin" data-date-format="yyyy-mm-dd" min='1899-01-01' required/>  
                            </div>
                        </div>
                        <div class="col-sm-6">    
                            <h5>Data e perfundimit te rezervimit</h5>
                            <div class="input-group">
                                <input type="date" id="checkout" name="checkout" data-date-format="yyyy-mm-dd" min='1899-01-01' required/>
                                
                            </div>
                        </div> 
                    </div>
                    <div class="marginForButton"></div>
                    <p></p>
                            <button type="submit" class="btn btn-primary" name="booking">Rezervo</button> 
                    </div>
                </div>    
            </div>
        </form>   
     
    </div>  
    <?php
    }   
}
?>
<?php
	include '../share/footer.php';
?>
</body>
</html>    