
 <?php
    include("../share/dbconnection.php"); 
    include("../share/header.php");
    include("../share/check_session.php");
    // include("check_booking.php");
    ?>
<!DOCTYPE html>
<html lang="en">

<body>
<div class="marginForButton"></div>
<?php
$sql = "SELECT * FROM room";

if($result=mysqli_query($con,$sql)) 
{ 
   //echo mysqli_num_rows($result);
    ?>

    <body>
    <div class="container">
    
    <?php 
	$id=$_GET['message_id'];
	if($id==1){
		echo "<div class='alert alert-warning'>Dhoma eshte e rezervuar	</div>";
	}
	elseif($id==2){
		echo "<div class='alert alert-success'> Rezervimi u krye me sukses.	</div>";
    }
    elseif($id==6){
		echo "<div class='alert alert-success'> Ju u loguat me sukses	</div>";
    }
	else {

	}
?>
        <div class="row">
            <?php


           while($row = $result->fetch_array()){
                
                ?>  
                <div class="col-md-4">
                <div class="thumbnail" id='room'>
                    <form action="booking.php" method="GET" id="booking" name="booking">
                        <div class="card">
                            <div class="card-header">    
                            </div>
                            <div class="card-body">
                            <!-- linku per images -->
                                <img class="card-img-top" src="../../public/img/room.jpeg" alt="room"> 
                                <p></p>
                                <h4 class="card-title">Details</h4>
                                <p></p>
                                <div class="form-group">
                                    <input id="room_id" name="room_id" type="hidden" value="<?= $row['room_id']  ?>">
                                    <p class="card-text">Numri i dhomes: <?= $row['room_number']  ?></p> 
                                </div>
                                    <p class="card-text">Tipi i dhomes: <?= $row['type']  ?></p>
                                    <p class="card-text">Cmimi i dhomes ne dollar: <?= $row['price']  ?></p>
                                    
                                <button type="submit" id="booking" class="btn btn-primary" name="booking">Rezervo</button>
                                
                            </div></form>
                        </div>   
                    
                </div> </div>
            <?php 
            }?>
       
   
<?php
}      
else 
{
?>
    <div class="alert alert-info">
        Nuk ka dhoma
    </div> </div>
<?php
}
?>
<?php
	include '../share/footer.php';
?>
</body>
</html>   