<?php
include("../share/header.php");
include("../share/dbconnection.php");
include("../share/check_session.php");
?>
<html>
	
<body >
	
<div class="container">

<?php
$sql = 'SELECT * FROM audit';
$result = $con->query($sql);
if ($result->num_rows > 0)
{
?>
	
	<h2>Audit</h2>
	<div id="example_wrapper" class="dataTables_wrapper">
	<div class="dataTables_length" id="example_length"></div>
	<div id="example_wrapper" class="dataTables_wrapper">

	<table id="example" name="id" class="table-bordered" class="display dataTable" style="width: 100%;" role="grid" aria-describedby="example_info">
	<thead>
		<tr  role="row">
		<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 92px;" aria-sort="ascending">Action</th>
		<th class="sorting_asc"tabindex="0" aria-controls="example" rowspan="1" colspan="1"  style="width: 92px;" aria-sort="ascending" >Table name</th>
		<th class="sorting_asc"tabindex="0" aria-controls="example" rowspan="1" colspan="1"  style="width: 92px;" aria-sort="ascending" >Modification date</th>
		<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  style="width: 92px;" aria-sort="ascending">Created_by</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($result as $row) { ?>
	<tr role="row" >
		<td class="sorting_1"><?php echo $row["action"]; ?></td>
		<td ><?php echo $row["table_name"]; ?></td>
		<td ><?php echo $row["created_date"]; ?></td>
		<td ><?php echo $row["created_by"]; ?></td>
			</tr>
<?php } ?> 
	</tbody>

	</table>	
	</div>
	</div>
	</div>

<?php } 
      $con->close();
?> 
	<script type="text/javascript" class="init" src="../../public/js/dataTable.js"></script>
	<script type="text/javascript" class="init" src="../../public/js/modal.js"></script>


	<?php
include '../share/footer.php';
?>

	</body>
	</html>



