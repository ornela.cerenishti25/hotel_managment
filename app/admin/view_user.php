<?php	
include("../share/dbconnection.php");
include("../share/header.php");
include("../share/check_session.php");
?>
<html>

<body>

        <div class="container">
	<?php 
	$id=$_GET['message_id'];
	if($id==1){
		echo "<div class='alert alert-success'>Veprimi juaj perfundoi me sukses	</div>";
	}
	elseif($id==2){
		echo "<div class='alert alert-warning'> Rekordi nuk u shtua.	</div>";
	}
	elseif($id==3){
		echo "<div class='alert alert-warning'> Rekordi nuk ndryshoi.	</div>";
	}
	elseif($id==4){
		echo "<div class='alert alert-warning'> Rekordi nuk u fshi. Useri mund te kete rezervuar	</div>";
    }
    
    
	else {

	}
?>
            <?php
            
			$sql = "SELECT * FROM user";
			$result = $con->query($sql);
            if ($result->num_rows > 0) 
            {
                // output data of each row
            ?>  
    <div id="example_wrapper" class="dataTables_wrapper">
	<div class="dataTables_length" id="example_length"></div>
	<div id="example_wrapper" class="dataTables_wrapper">

	<table id="example" name="id" class="table-bordered" class="display dataTable" role="grid" aria-describedby="example_info">
                    <thead>
                        <tr role="row">
                            <th>Id </th>
                            <th>Perdoruesi </th>
                           
                            <th>Email </th>
                            <th>Emri </th>
                            <th>Celulari</th>
                            <th>Roli</th>
                            <th>Ndrysho </th>
                            <th>Fshi </th>
                        </tr>
                    </thead> 
                    <tbody>
                    <?php foreach ($result as $row) { ?>
                                <tr role="row" >
                                    <td class="sorting_1"> <?= $row['user_id']  ?> </td>
                                    <td> <?= $row['username'] ?> </td>
                                   
                                    <td> <?= $row['email'] ?> </td>
                                    <td> <?= $row['name'] ?> </td>
                                    <td> <?= $row['cellphone'] ?> </td>
                                    <td> <?= $row['description'] ?> </td>
                                    <td> <?= "<a href='update_user.php?user_id=" . $row['user_id'] ."'><button type='button' class='btn btn-warning' name='update'> Ndrysho </button></a>" ?> </td> 
                                    <td ><input type="submit" class="btn btn-danger" value="Fshi" onclick='delete_data("<?=$row['user_id']; ?>")' ></td>                          
                            </tr>                          
                            <?php
                            }
                            ?>                    
                    </tbody>                  
                </table>
                        </div></div>
            <?php
            } 
          
                
            ?>
                    
            <a class="btn btn-primary" href="add_user.php" role="button">Shto perdorues</a>
            <?php
      $con->close();
?> 
        <script type="text/javascript" class="init" src="../../public/js/dataTable.js"></script>
    <script type="text/javascript" class="init" src="../../public/js/modal.js"></script>
        <!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
			<form action="delete_user.php" method="post">
        <div class="form-group">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Fshirja e userit</h4>
        </div>
        <div class="form-group">
          <p>Jeni i sigurt qe doni te fshihni perdoruesin?</p><br><br>
        </div>
        <div class="form-group">
				<input type="hidden" name = "id" id = "id"/>    
				<a id="button_anullo" class="btn btn-basic" href="view_user.php" role="button">Anullo </a> 
        <input type="submit" class="btn btn-danger" name="delete" value="Fshi" /> 

				</div>
			</form>
      </div>
    </div>
  </div>
</div>
<?php
include '../share/footer.php';
?>                 
    </body>
	</html>