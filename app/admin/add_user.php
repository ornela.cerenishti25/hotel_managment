<?php 
include("../share/header.php");
include("../share/dbconnection.php");
include("../share/check_session.php");
?>
<!DOCTYPE html>
<html>
<body>
  
<div class="container">
<form action="check_add_user.php" method="post">
  <div class="input-container">
    <i class="fa fa-user icon"></i>
    <input class="input-field" type="text" placeholder="Username" name="username" maxlength="20" required>
  </div>

  <div class="input-container">
    <i class="fa fa-lock icon"></i>
    <input class="input-field" type="password" placeholder="Password" name="password" maxlength="25" required>
  </div>
  <div class="input-container">
    <i class="fa fa-envelope icon"></i>
    <input class="input-field" type="email" placeholder="Email" name="email" autocomplete="off" required>
  </div>
  <div class="input-container">
    <i class="fa fa-user icon"></i>
    <input class="input-field" type="text" placeholder="Emri" name="name" required maxlength="20">
  </div>
  <h3>Zgjidh rolin</h3>
  <div class="input-container">
    <i class="fa fa-user-secret icon"></i>
    <select class="input-field" name="role" id="role">
        <option id="role">User</option>
        <option id="role">Admin</option>
    </select>
  </div>
  <button type="submit" class="btn-primary" value="addUser" name="addUser">Shto</button>
  <div class="additional-info">
  </div>
</form>
</div>
<?php
include '../share/footer.php';
?>
</body>
</html>
