
<!DOCTYPE html>


<html lang="en">
  <head>
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="../../public/css/login.css">
  </head>
  
  <body><br><br>
<div class="container">
  <?php 
	$id=$_GET['message_id'];
 if($id==2){
		echo "<div class='alert alert-warning'> Vendosni kredencialet e duhura.	</div>";
	}
  elseif($id==1){
		echo "<div class='alert alert-success'> Ju u regjistruat me sukses. Vendosni kredencialet per tu loguar	</div>";
  }
  elseif($id==3){
		echo "<div class='alert alert-success'> Passwordi i ri u dergua ne email</div>";
	}
	else {

	}
?>
<form action="check_login.php" method="POST" name="login">
  
  <div class="input-container">
    <i class="fa fa-user icon"></i>
    <input class="input-field" type="text" placeholder="Fut emrin e perdoruesit" name="username" maxlength="20" required>
  </div>
  <div class="input-container">
    <i class="fa fa-lock icon"></i>
    <input class="input-field" type="password" placeholder="Fut fjalekalimin" name="password" size="25" maxlength="20" required>
  </div>
  <div class="input-container">
  <button type="submit" class="btn-primary" name="login">Hyr</button>
  </div>
  <div class="additional-info">
  <p> Nuk je regjistruar
  <button type="button" class="btn-reg"><a href="register.php">Regjistrohu</a></button>
  Ke harruar fjalekalimin
  <button type="button" class="btn-extra"><a href="change_password.php">Kliko Ketu</a></button>
   </p>
  </div>
</form>
</div>

</body>
</html>