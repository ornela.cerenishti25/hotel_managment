<?php	
include("../share/header.php");
include("../share/dbconnection.php");
include("../share/check_session.php");


?>
<html>

<body >
<div class="marginForButton"></div>
<div class="container">	
<?php 
	$id=$_GET['message_id'];
	if($id==1){
		echo "<div class='alert alert-success'>Veprimi juaj perfundoi me sukses	</div>";
	}
	elseif($id==2){
		echo "<div class='alert alert-info'> Rekordi nuk u shtua.	</div>";
	}
	elseif($id==3){
		echo "<div class='alert alert-info'> Rekordi nuk ndryshoi.	</div>";
	}
	elseif($id==4){
		echo "<div class='alert alert-info'> Rekordi nuk u fshi. Punonjesi mund te kete kryer rezervim	</div>";
	}
	else {

	}
?>

<?php
$sql = 'SELECT * FROM employee';
$result = $con->query($sql);
if ($result->num_rows > 0)
{
?>
	
	
	<div id="example_wrapper" class="dataTables_wrapper">
	<div class="dataTables_length" id="example_length"></div>
	<div id="example_wrapper" class="dataTables_wrapper">

	<table id="example" name="id" class="table-bordered" class="display dataTable"  role="grid" aria-describedby="example_info">
	<thead>
		<tr  role="row">
		<th>Employee_id</th>
		<th >Employee_name</th>
		<th >Job</th>
		<th >Salary</th>
		<th >Ndrysho</th>
		<th >Fshi</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($result as $row) { ?>
	<tr role="row" >
		<td class="sorting_1"><?php echo $row["employee_id"]; ?></td>
		<td ><?php echo $row["name"]; ?></td>
		<td ><?php echo $row["job"]; ?></td>
		<td ><?php echo $row["salary"]; ?></td>
		<td > <a class="btn btn-warning" href='update_employee.php?employee_id=<?= $row["employee_id"] ?>'role='button' >Ndrysho</a> 
		<td ><input type="submit" class="btn btn-danger" value="Fshi" onclick='delete_data("<?=$row['employee_id']; ?>")' ></td>

	</tr>
<?php } ?> 
	</tbody>

	</table>	

	
	</div></div>
	<?php } 
     
?>
<a class="btn btn-primary" href='add_employee.php'role='button'>Shto punonjes</a> 
<?php 
      $con->close();
?> 
	<script type="text/javascript" class="init" src="../../public/js/dataTable.js"></script>
	<script type="text/javascript" class="init" src="../../public/js/modal.js"></script>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
			<form action="delete_employee.php" method="post">
        <div class="form-group">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Fshirja e punonjesit</h4>
        </div>
        <div class="form-group">
          <p>Jeni i sigurt qe doni te fshihni punonjesin?</p><br><br>
        </div>
        <div class="form-group">
				<input type="hidden" name = "id" id = "id"  />    
				<a id="button_anullo" class="btn btn-basic" href="read_employee.php" role="button">Anullo </a> 
        <input type="submit" class="btn btn-danger" name="fshi" value="Fshi" /> 

				</div>
			</form>
      </div>
    </div>
  </div>

</div>

</div>
	<?php
include '../share/footer.php';
?>

	</body>
	</html>
